<?php
require_once("bootstrap.php");


  if(isset($_POST["email"]) && isset($_POST["password"])){

      $login_result = $dbh->checkLogin($_POST["email"], $_POST["password"]);
    
      if(count($login_result)==0 ){
          $templateParams["errorelogin"] = "Errore! Username o password non corretti";
      }
      else{
          registerLoggedUser($login_result[0]);
      }
    
  }

    if(isUserLoggedIn()){
      header("Location: ./index.php");
    } 
    if(isVenditoreLoggedIn()){
      header("Location: ./venditore.php");
    }
    else{
      $templateParams["titolo"] = "Eletronics - Admin";
      $templateParams["main"] = "login-form.php";
    }



require("template/base.php");
?>