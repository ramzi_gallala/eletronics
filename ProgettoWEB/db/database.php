<?php

class DatabaseHelper{
    
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if($this->db->connect_error){
            die("Connessione al database fallita...");
        }
    }

    public function getArticles(){

        $stmt = $this->db->prepare("SELECT ID_Articolo, titolo, descrizione, prezzo, immagine, quantita FROM articoli");
        $stmt->execute();

        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getArticlesByVenditore($idVenditore){
        $stmt = $this->db->prepare("SELECT ID_Articolo, titolo, descrizione, prezzo, quantita, immagine FROM articoli WHERE articoli.ID_utente = ?;");
        $stmt->bind_param("i", $idVenditore);
        $stmt->execute();

        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function insertArticle($titolo, $descrizione, $prezzo, $quantita, $immagine, $idUtente){
        var_dump($_POST["immagine"]);
        $query = "INSERT INTO `articoli` (`ID_Articolo`, `titolo`, `descrizione`, `prezzo`, `quantita`, `immagine`, `ID_utente`) VALUES (NULL, ?, ?, ?, ?, ?, ?);";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssdisi',$titolo, $descrizione, $prezzo, $quantita, $immagine, $idUtente);
        $stmt->execute();

    }

    public function deleteArticleByID($idArticolo){
        $query = "DELETE FROM articoli WHERE articoli.ID_Articolo = ? ;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idArticolo);
        $stmt->execute();

    }

    public function updateQuantity($idUtente, $idArticolo, $quantita){
        $query = "UPDATE articoli SET quantita = ? WHERE articoli.ID_utente = ? AND articoli.ID_Articolo = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("iii", $quantita, $idUtente, $idArticolo);
        $stmt->execute();
    }

    //Se articolo non è già presente, lo inserisco ex novo
    public function addToCarrello($idArticolo, $idUtente){
        $query = "INSERT INTO `carrello` (`ID_Carrello`, `ID_Articolo`, `ID_Utente`, `Quantità`) VALUES (NULL, ?, ?, '1' );";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii", $idArticolo, $idUtente);
        $stmt->execute();
    }

    //Se articolo già presente nel carrello ---> incremento +1 la quantita
    public function changeQuantity($idUtente, $idArticolo, $quantita){
        $query = "UPDATE carrello SET Quantità = ? WHERE carrello.ID_Utente = ? AND carrello.ID_Articolo = ?"; //query di UPDATE
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("iii", $quantita, $idUtente, $idArticolo);
        $stmt->execute();
    }

    public function getQuantityCarrello($idArticolo, $idUtente){
        $query = "SELECT Quantità FROM carrello WHERE carrello.ID_Utente = ? AND carrello.ID_Articolo = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii", $idUtente, $idArticolo);
        $stmt->execute();

        $result = $stmt->get_result();

        return $result->fetch_row();



    }

    public function correctQuantity($idArticolo){
        echo "ID_Articolo: "; echo $idArticolo;
        $query = "UPDATE articoli SET quantita = quantita - '1' WHERE articoli.ID_Articolo = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idArticolo);
        $stmt->execute();
    }

    /*FUNZIONI UTILI AL LOGIN E REGISTRAZIONE*/
    public function checkLogin($username, $password){
        $stmt = $this->db->prepare("SELECT ID_Utente, Nome, Cognome, Venditore FROM utenti WHERE Email = ? AND Password = ?");
        $stmt->bind_param("ss", $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function addUser($lname, $fname, $email, $password, $Venditore){
        $stmt = $this->db->prepare("INSERT INTO `utenti` (`ID_Utente`, `Nome`, `Cognome`, `Email`, `Password`, `Venditore`) VALUES (NULL, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssi", $lname, $fname, $email, $password, $Venditore);
        $stmt->execute();
    }
    
    public function Products($idUtente, $idOrdineG){
        $stmt = $this->db->prepare("SELECT prezzo, ordini.Quantità, articoli.titolo, articoli.immagine 
                                    FROM ordini,articoli 
                                    WHERE ordini.ID_Articolo=articoli.ID_Articolo AND ordini.ID_Utente = ? AND ordini.ID_ordini_g=?");
        $stmt->bind_param("ii", $idUtente, $idOrdineG);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function Preview($idUtente){
        $stmt = $this->db->prepare("SELECT Somma, ordini.ID_ordini_g
                                    FROM ordini_g, ordini
                                    WHERE ordini_g.ID_ordini_g=ordini.ID_ordini_g and ordini.ID_Utente=?
                                    GROUP BY ordini.ID_ordini_g
                                    ORDER BY ordini.ID_ordini_g DESC");
        $stmt->bind_param("i", $idUtente);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function checkProducts($idUtente){
        $stmt = $this->db->prepare("SELECT titolo,ID_Articolo, prezzo
                                    FROM articoli
                                    WHERE ID_Utente=? AND quantita=0");
        $stmt->bind_param("i", $idUtente);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //funzioni relative alla pagina del carrello
    public function getCarrello($userID){
        $stmt = $this->db->prepare("SELECT ID_Carrello,carrello.ID_Utente, carrello.ID_Articolo, titolo, descrizione, prezzo, immagine, carrello.Quantità 
                                    FROM carrello INNER JOIN utenti ON carrello.ID_Utente = utenti.ID_Utente 
                                    INNER JOIN articoli ON carrello.ID_Articolo = articoli.ID_Articolo 
                                    WHERE utenti.ID_Utente = ?
                                    ORDER BY ID_Carrello DESC");
        $stmt->bind_param("i", $userID);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC); 
    }

    public function getSpesaCarrelloTot($userID){
        $stmt = $this->db->prepare("SELECT carrello.Quantità,prezzo
                                    FROM carrello INNER JOIN utenti ON carrello.ID_Utente = utenti.ID_Utente 
                                    INNER JOIN articoli ON carrello.ID_Articolo = articoli.ID_Articolo 
                                    WHERE utenti.ID_Utente = ?
                                    GROUP BY carrello.ID_Utente, carrello.ID_Articolo");
        $stmt->bind_param("i", $userID);
        $stmt->execute();
        $result = $stmt->get_result();
        $array = $result->fetch_all(MYSQLI_ASSOC);
        $spesa = 0;
        foreach($array as $value){
            $spesa += $value["Quantità"]*$value["prezzo"];
        }
        return $spesa; 
    }

    public function removeArticoloCarrello($idCarr, $idArt, $idUser){
        $carrello = $this->getCarrello($idUser);
        foreach($carrello as $articolo){
            if($articolo["ID_Articolo"] == $idArt){
                if($articolo["Quantità"] > 1){
                    $stmt = $this->db->prepare("UPDATE carrello 
                                                SET Quantità = Quantità - 1
                                                WHERE ID_Articolo = ? AND ID_Utente = ?");
                    $stmt->bind_param("ii", $idArt, $idUser);
                    $stmt->execute();

                    $stmt2 = $this->db->prepare("UPDATE articoli 
                                                 SET quantita = quantita+1 
                                                 WHERE ID_Articolo = ?");
                    $stmt2->bind_param("i", $idArt);
                    $stmt2->execute();
                }
                else{
                    $stmt = $this->db->prepare("DELETE 
                                                FROM carrello 
                                                WHERE ID_Carrello = ?");
                    $stmt->bind_param("i", $idCarr);
                    $stmt->execute();
                    
                    $stmt2 = $this->db->prepare("UPDATE articoli 
                                                SET quantita = quantita+1 
                                                WHERE ID_Articolo = ?");
                    $stmt2->bind_param("i", $idArt);
                    $stmt2->execute();
                }
            }
        }
        
    }

    public function svuotaCarrello($idUser,$spesaTot){
        $carrello = $this->getCarrello($idUser);
        $stmt = $this->db->prepare("INSERT INTO ordini_g (Somma)
                                    VALUES (?)");
        $stmt->bind_param("d",$spesaTot);
        $stmt->execute();

        $stmt2 = $this->db->prepare("SELECT ID_ordini_g
                                     FROM ordini_g
                                     ORDER BY ID_ordini_g DESC
                                     LIMIT 1");
        $stmt2->execute();
        $res = $stmt2->get_result();
        $ordine_g_ID = $res->fetch_row();
        


        foreach($carrello as $articolo){
            $dataSped = date("Y-m-d H:i:s");
            $stmt3 = $this->db->prepare("INSERT INTO ordini (ID_Articolo,ID_Utente,Quantità,Stato,Data, ID_ordini_g)
                                        VALUES (".$articolo["ID_Articolo"].",?,".$articolo["Quantità"].",'Spedito','".$dataSped."',".$ordine_g_ID[0].")");
            $stmt3->bind_param("i", $idUser);
            $stmt3->execute();

            $stmt4 = $this->db->prepare("DELETE 
                                         FROM carrello 
                                         WHERE ID_Utente = ?");
            $stmt4->bind_param("i",$idUser);
            $stmt4->execute();

        }
        
    }
    

    //funzioni relative agli ordini

    public function getOrdini($idUser){
        $stmt = $this->db->prepare("SELECT ID_Ordini,ordini.ID_Utente, ordini.ID_Articolo, titolo, immagine, ordini.Quantità,articoli.prezzo, Stato, Data 
                                    FROM ordini INNER JOIN utenti ON ordini.ID_Utente = utenti.ID_Utente 
                                    INNER JOIN articoli ON ordini.ID_Articolo = articoli.ID_Articolo 
                                    WHERE utenti.ID_Utente = ?
                                    ORDER BY ID_Ordini DESC
                                    LIMIT 10");
        $stmt->bind_param("i", $idUser);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function cambiaStato($ID_Ordine,$newStato){
        $stmt = $this->db->prepare("UPDATE ordini
                                    SET Stato = ?
                                    WHERE ID_Ordini = ?");
        $stmt->bind_param("si", $newStato, $ID_Ordine);
        $stmt->execute();
    }
}
    
?>