<?php 
    $flagBg=true;
    foreach($templateParams["Ordini"] as $articolo):
        $colore =($flagBg ? "bg-light" : "bg-secondary");
?>


    <div class="row <?php echo $colore; ?>">  
        <article class="artcl pt-3">
            <div class="row">
                <?php $flagBg = !$flagBg; ?>
                <div class="col-3">                                                   
                    <a target="_blank" href="./images/articles/<?php echo $articolo["immagine"] ?>" ><img class="imgArt img-fluid pl-4 pb-3" src="./images/articles/<?php echo $articolo["immagine"] ?>" alt="<?php echo $articolo["titolo"] ?>" /></a>
                </div>
                <div class="col-9">
                    <header>                                
                        <h2 class="titleArt text-info font-weight-bold"><?php echo $articolo["titolo"] ?></h2>
                    </header>
                    <div class="divDati row">
                        <div class="col-12">               
                            <div>
                                <label class="lblOrdini font-weight-bold">Data spedizione:</label>
                                <label class="lblOrdini"><?php echo (explode(" ",$articolo["Data"])[0]) ?></label>
                            </div>
                            <div class="sectOrdini">
                                <label class="lblOrdini font-weight-bold">Stato:</label> 
                                <label class="lblOrdini"> <?php echo $articolo["Stato"] ?></label>
                            </div>
                            <div class="sectOrdini">
                                <label class="lblOrdini font-weight-bold">Prezzo:</label>
                                <label class="lblOrdini">€<?php echo $articolo["prezzo"] ?></label>
                            </div>
                            <div >
                                <label class="lblOrdini font-weight-bold">Quantità:</label>
                                <label class="lblOrdini">x<?php echo $articolo["Quantità"] ?></label>
                            </div> 
                        </div>              
                    </div>
                </div>
         </div>
        </article> 
       
    </div> 

<?php endforeach; ?>