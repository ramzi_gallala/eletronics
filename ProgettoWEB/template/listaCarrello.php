
<?php 
    $flagBg=true;
    $ID_Utente;
    $idLbl = 0;
    foreach($templateParams["articoli_carrello"] as $articolo): 
?>           
<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<?php $GLOBALS['ID_Utente'] = $articolo["ID_Utente"]; ?>    
        <div class="row <?php echo ($flagBg ? "bg-light" : "bg-secondary"); ?>">   
            <article class="artcl pt-3 "> 
                <?php $flagBg = !$flagBg; ?> 
                <div class="row">
                    <div class="col-3 ">                                                 
                        <a target="_blank" href="./images/articles/<?php echo $articolo["immagine"] ?>" ><img class="imgArt img-fluid pb-3 pl-4" src="./images/articles/<?php echo $articolo["immagine"]?>" alt="<?php echo $articolo["titolo"] ?>" /></a>
                    </div>
                    <div class="divDescr col-9">
                        <header>                                
                            <h2 class="titleArt text-center text-info font-weight-bold"><?php echo $articolo["titolo"] ?></h2>
                        </header>                       
                        <div class="col-12">
                            <div>
                                <p class="descr "><?php echo $articolo["descrizione"] ?></p>
                            </div>
                            <div class="divArtCarr pt-5 mt-5">
                                <div class="col-12">
                                    <label class="lblPrezzo lblCarrello font-weight-bold">Prezzo Articolo:</label>
                                    <label class="lblCarrello">€<?php echo $articolo["prezzo"] ?></label>                     
                                </div>
                                <div class="col-12">
                                    <label class="lblCarrello font-weight-bold">Quantità:</label>
                                    <label class="lblCarrello">x<?php echo $articolo["Quantità"] ?></label>
                                </div>
                                <div class="col-12 text-center py-5">
                                    <label class="invisible" for="remove<?php echo $idLbl ?>">s</label><input id="remove<?php echo $idLbl++ ?>" type="submit"  class="btnRemove btn-danger btn-sm" value="Rimuovi articolo"/>
                                    <label class="invisible" for="remove<?php echo $idLbl ?>">s</label><input id="remove<?php echo $idLbl++ ?>" style="display:none;" name="ID_Carrello" value="<?php echo $articolo["ID_Carrello"] ?>" />
                                    <label class="invisible" for="remove<?php echo $idLbl ?>">s</label><input id="remove<?php echo $idLbl++ ?>" style="display:none;" name="ID_Articolo" value="<?php echo $articolo["ID_Articolo"] ?>" />
                                    <label class="invisible" for="remove<?php echo $idLbl ?>">s</label><input id="remove<?php echo $idLbl++ ?>" style="display:none;" name="ID_Utente" value="<?php echo $articolo["ID_Utente"] ?>" />
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>                        
        </div> 
    
</form>               
<?php endforeach; ?>
<div class="row mt-5">
    <div class="col-6 text-right">
        <label class="lblCarrello font-weight-bold">Prezzo totale:</label>
    </div>    
    <div class="col-6">
        
        <label id="totPrice" class="lblCarrello">€<?php echo $templateParams["spesaCarrello"] ?></label>    
    </div>
</div>
<div class="row">
    <div class="col-12 text-center py-2">
        <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <label class="invisible" for="buy">s</label><input id="buy" type="submit" class="btnBuy  btn-primary btn-lg font-weight-bold" <?php echo($templateParams["spesaCarrello"] == 0 ? "disabled" : "") ?> value="Ordina e Paga"/> 
            <label class="invisible" for="buy1">s</label><input id="buy1" style="display:none;" name="ID_Utente" value="<?php echo $ID_Utente ?>"/>
            <label class="invisible" for="buy2">s</label><input id="buy2" style="display:none;" name="SpesaTot" value="<?php echo $templateParams["spesaCarrello"] ?>" />
        </form>
        
    </div>   
</div>
<div class="push"> </div>


