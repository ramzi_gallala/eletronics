<!DOCTYPE html>
<html lang="it">
    <head>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./style/style.css" >
        <link rel="stylesheet" href="./style/venditoreStyle.css">
        <link rel="stylesheet" href="./style/articleStyle.css">
        <link rel="stylesheet" href="./style/listeArticoliStyle.css" >
        <title>ELECTRONICS</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 px-0 bg-info">
                    <header>
                        <?php if(!empty($_SESSION["venditore"])): ?>
                            <a href="./venditore.php" title="Go to main page">
                                <img class="img-fluid col-10 float-left pr-0 pl-5" src="./images/others/logo.png" id="logo" alt="LogoElectronics"/>
                            </a>
                        <?php else: ?>
                            <a href="./index.php" title="Go to main page">
                                <img class="img-fluid col-10 float-left pr-0 pl-5" src="./images/others/logo.png" id="logo" alt="LogoUtente"/>
                            </a>
                        <?php endif; ?>
                            <a <?php if(isset($_SESSION["ID_Utente"])){
                                        echo "href='./index.php?ref_=xzw23z7va' title='Logout'";
                                    }else{
                                        echo "href='./login.php' title='Login'";

                                    } ?>>
                                <img class="img-fluid col-2 float-right pt-5" src="./images/others/<?php echo $templateParams["imgLog"]; ?>" id="user" alt="User"/>
                            </a>
                    </header>
                </div>
            </div>
            <div class="row border-top  border-bottom border-primary">
                <div class="col-12 px-0">
                    <ul class="nav">
                        <li class="nav-item col-4 border-right btn-secondary">
                            <a class="nav-link mt-1 text-center px-0 font-weight-bold text-white" href="./notifiche.php">Notifiche</a>
                        </li>
                        <?php if(empty($_SESSION["venditore"])): ?>
                            <li class="nav-item col-4 border-right btn-secondary">
                                <a class="nav-link mt-1 text-center px-0 font-weight-bold text-white" href="./ordini.php">Ordini</a>
                            </li>
                            <li class="nav-item col-4 btn-secondary">
                                <a class="nav-link mt-1 text-center px-0 font-weight-bold text-white" href="./carrello.php">Carrello</a>
                            </li>
                        <?php else: ?>
                            <li class="nav-item col-8 border-right btn-secondary">
                                <a class="nav-link mt-1 text-center px-0 font-weight-bold text-white" href="./venditore.php">Miei prodotti</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <main>
                <?php require($templateParams["main"]); ?>
            </main>
            <div class="push"> </div>
            <div class=" row">
                    <div class="divFooter col-12 bg-dark">
                        <footer class=" py-3 text-white">
                            <h2 class="text-center col-12">Electronics S.r.l.</h2>
                            <div class="row mx-0 ">
                                <div class="col-6 text-center">
                                    <h3>Chi siamo</h3>
                                    <pre class="text-white">
    Davide Cellot
    Davide Degli Esposti
    Ramzi Gallala
                                    </pre>
                                </div>
                                <div class="col-6 text-center">
                                    <h3>Dove siamo</h3>
                                    <pre class="text-white">
    Cesena (FC) 47521
    Via Niccolò Macchiavelli
                                    </pre>
                                </div>
                            </div>
                        </footer>
                    </div>
            </div>
        </div>
    </body>
</html>