<div class="row">
                <div class="col-12">
                        <form  action="#" method="POST" class="col-12 mt-4 px-5">
                            <h2 class="mb-5">Login</h2>
                          <?php if(isset($templateParams["errorelogin"])): ?>
                          <p>
                            <?php echo $templateParams["errorelogin"];?>
                          </p>
                          <?php endif; ?>
                            <div class="form-group row text-center">
                                <label for="email" class="col-6">Email</label>
                                <input type="email" class="form-control col-6" id="email"  name="email" placeholder="Inserisci email" required>
                            </div>
                            <div class="form-group row text-center">
                                <label for="password" class="col-6">Password</label>
                                <input type="password" class="form-control col-6" id="password" name="password" placeholder="Inserisci Password" required>
                            </div>
                            <div class="form-group text-right mb-2">
                                <button type="submit" class="btn btn-light">Entra</button>
                            </div>
                        </form>
                        <div class="col-12 text-center pb-3"><a href="newUser.php">Registrati Qui!!</a></div>
                 </div>
            </div>