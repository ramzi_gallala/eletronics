<div class="row">
                <div class="col-12 col-md-10 pl-1">
                    <form action="#" method="POST" class="mt-4 mb-4 pl-2 pr-4 py-4">
                        <h2 class="mb-5">Registrazione</h2>
                        <div class="form-group row text-center">
                            <label for="fname" class="col-6">Nome</label>
                            <input type="text" class="form-control col-6" id="fname" name="fname" placeholder="Inserisci nome" required>
                        </div>
                        <div class="form-group row text-center">
                            <label for="lname" class="col-6">Cognome</label>
                            <input type="text" class="form-control col-6" id="lname" name="lname" placeholder="Inserisci cognome" required>
                        </div>
                        <div class="form-group row text-center">
                            <label for="email" class="col-6">Email</label>
                            <input type="email" class="form-control col-6" id="email" name="email" placeholder="Inserisci email" required>
                        </div>
                        <div class="form-group row text-center">
                            <label for="password" class="col-6">Password</label>
                            <input type="password" class="form-control col-6" id="password" name="password" placeholder="Inserisci Password" required>
                        </div>
                        <div class="form-group row text-center">
                            <div class="col-6"></div>
                            <input type="checkbox" class="form-check mt-1" id="venditore" name="venditore" value="1">
                            <label for="venditore" class=" pl-3"> Sono un Venditore</label>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-light">Invia</button>
                        </div>
                    </form>
                </div>
</div>