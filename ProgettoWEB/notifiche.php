<?php
require_once("./bootstrap.php");
    
    if(isset($_SESSION["ID_Utente"])){
      $templateParams["titolo"] = "Eletronics - Admin";
      if($_SESSION["venditore"]==0){
        $login_result = $dbh->Preview($_SESSION["ID_Utente"]);
      }
      else{
        $login_result = $dbh->checkProducts($_SESSION["ID_Utente"]);
      }
        
      $templateParams["previews"] = $login_result;
      $templateParams["main"] = "notifiche-form.php";
      require("template/base.php");
	  }
    else{
      header("Location: ./login.php");
    }
    
?>