<?php

    require_once("./bootstrap.php");

    $idVenditore = $_SESSION["ID_Utente"];

    if(isset($_POST["titolo"]) && ($_POST["titolo"] != "")){
        $dbh->insertArticle($_POST["titolo"], $_POST["descrizione"], $_POST["prezzo"], $_POST["quantita"], $_POST["immagine"], $idVenditore);
    }
    if(!empty($_POST)){
        $result = $_POST;
        $id_articolo = key($result);
        $newQuantita = $_POST[$id_articolo];
        $dbh->updateQuantity($idVenditore, $id_articolo, $newQuantita);
    }
    

    $templateParams["main"] = "venditore-main.php";

    
    $templateParams["articoli"] = $dbh->getArticlesByVenditore($idVenditore);

    require("template/base.php");

?>