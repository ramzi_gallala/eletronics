<?php

    require_once("./bootstrap.php");

    //action = 1 ---> elimino l'articolo tramite l'id
    if($_GET["action"] == 1){
        
        $idArticolo = $_GET["id"];
        $dbh->deleteArticleByID($idArticolo);
        header("location: venditore.php");
    }

   
    //action = 3 ---> inserire articolo nel carrello
    if($_GET["action"] == 3){
        $idArticolo = $_GET["id"];
        $idUtente = $_SESSION["ID_Utente"];

        $quantita= $dbh->getQuantityCarrello($idArticolo, $idUtente);

        if(!empty($quantita)){            
            //Articolo giù presente nel carrello ---> incremento la quantità
            $quantita[0] ++ ;
            $dbh->changeQuantity($idUtente, $idArticolo, $quantita[0]);
            $dbh->correctQuantity($idArticolo);
        } else{
            //Articolo non presente, inserisco un nuovo articolo nel carrello
            $dbh->addToCarrello($idArticolo, $idUtente);
            $dbh->correctQuantity($idArticolo);
        }

        header("location: index.php");
    }

?>