<?php

require_once("./bootstrap.php");

    if(isset($_SESSION["ID_Utente"])){

        $idUser = $_SESSION["ID_Utente"]; 

        $templateParams["Ordini"] = $dbh->getOrdini($idUser);
        foreach($templateParams["Ordini"] as $articolo){
            $timeOld = strtotime($articolo["Data"]); 
            $timeNow = strtotime("now"); 
            if($timeNow >= $timeOld + 20){
                $dbh->cambiaStato($articolo["ID_Ordini"],"In Transito");
            }
            if($timeNow >= $timeOld + 40){
                $dbh->cambiaStato($articolo["ID_Ordini"],"Conseganto");
                
            } 
        }

        $templateParams["main"] = "listaOrdini.php";
        $templateParams["Ordini"] = $dbh->getOrdini($idUser);


        require("template/base.php");
    } else{
        header("Location: ./login.php");
    }


?>