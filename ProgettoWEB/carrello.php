<?php

require_once("./bootstrap.php");

    
    
    if(isset($_SESSION["ID_Utente"])){
        
        $idUser = $_SESSION["ID_Utente"];
        
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            if(isset($_POST["ID_Carrello"]) && isset($_POST["ID_Articolo"]) && isset($_POST["ID_Utente"])){
                $dbh->removeArticoloCarrello($_POST["ID_Carrello"], $_POST["ID_Articolo"], $_POST["ID_Utente"]);
            }
            if(isset($_POST["ID_Utente"]) && !isset($_POST["ID_Carrello"]) && !isset($_POST["ID_Articolo"])){
                $dbh->svuotaCarrello($_POST["ID_Utente"],$_POST["SpesaTot"]);

            }
            
        }
        
        $templateParams["main"] = "listaCarrello.php";
        $templateParams["articoli_carrello"] = $dbh->getCarrello($idUser);
        $templateParams["spesaCarrello"] = $dbh->getSpesaCarrelloTot($idUser);
        
        require("template/base.php");
    
    }else{
        header("Location: ./login.php");
    }


?>